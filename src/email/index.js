const nodemailer = require("nodemailer");
const path = require("path");
const fs = require("fs");

require("dotenv").config();
const template = "email.html";

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST || "smtp_host",
  port: 587,
  secure: false,
  auth: {
    user: process.env.SMTP_USER_ACCOUNT || "user@accpunt.com",
    pass: process.env.SMTP_PASSWORD || "pass",
  },
  tls: {
    rejectUnauthorized: false,
  },
});

const sendEmail = (mailOptions) => {
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

async function readFile(template) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(__dirname, template), "utf8", function (
      err,
      data
    ) {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

const composeAndSendEmail = (to) => {
  readFile(template)
    .then((mailBody) => {
      var mailOptions = {
        from: "presales@applogical.es",
        to,
        subject: "Recovery password email for Presales Toolkit",
        html: mailBody,
        attachments: [
          {
            filename: "honey-small.png",
            path: __dirname + "/honey-small.png",
            cid: "logo",
          },
        ],
      };
      sendEmail(mailOptions);
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = { composeAndSendEmail };
