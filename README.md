# accepts

Sample use of nodemailer.

## Use

This is a single app for testing of the nodemailer

Before to use, configure .env file with:

SMTP_PASSWORD= This is the password for the smtp server.

SMTP_USER_ACCOUNT= This is the user account for the smtp server.

SMTP_HOST= This is the smtp server address.

## License

[MIT](LICENSE)
